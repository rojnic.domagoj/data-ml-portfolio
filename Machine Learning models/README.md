# About
Python implementation of most commonly used Machine Learning algorithms.
<br><br>
**Note:** This is not a new or optimized approach. The puropose of this project is to present the inner workings of algorithms in a transparent way and to get better insights of how the algorithms work "under the hood".