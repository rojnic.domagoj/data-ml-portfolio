# Data Science & Machine Learning portfolio
A collection of data science and machine learning projects to demonstrate skillset, knowledge, and topics I'm interested in.

## Content
* **SQL & PowerBI**: Statistical analysis of Olympics with formulated SQL queries and visualizations using PowerBI
* **Machine Learning models** - implementation of most commonly used machine learning models with some algorithms implemented from scratch
* **ETL Pipeline** - data pipeline which aggregates data stored in Elastisearch and saves it to Postgres database, using Airflow and Python
* **PythonWebScraping + EDA** - Web scraping of Indeed job postings using Selenium and beautifulsoup, with data processing and exploritory data analysis